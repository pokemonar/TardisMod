package net.tardis.api.events;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraftforge.eventbus.api.Event;
/**
 * <p>Allows modders to render objects after the Forge RenderWorldLastEvent</p>
 *
 * <p>This event fired in {@link GameRendererMixin#renderWorld}, after the player hands are rendered, on the
 * {@link net.minecraftforge.common.MinecraftForge#EVENT_BUS} only on the
 * {@linkplain net.minecraftforge.fml.LogicalSide#CLIENT client-side}.
 */
public class TardisWorldLastRender extends Event
{
    private final WorldRenderer context;
    private final MatrixStack mat;
    private final float partialTicks;
    private final Matrix4f projectionMatrix;
    private final long finishTimeNano;

    public TardisWorldLastRender(WorldRenderer context, MatrixStack mat, float partialTicks, Matrix4f projectionMatrix, long finishTimeNano)
    {
        this.context = context;
        this.mat = mat;
        this.partialTicks = partialTicks;
        this.projectionMatrix = projectionMatrix;
        this.finishTimeNano = finishTimeNano;
    }

    public WorldRenderer getContext()
    {
        return context;
    }

    public MatrixStack getMatrixStack()
    {
        return mat;
    }

    public float getPartialTicks()
    {
        return partialTicks;
    }

    public Matrix4f getProjectionMatrix()
    {
        return projectionMatrix;
    }

    public long getFinishTimeNano()
    {
        return finishTimeNano;
    }
}
