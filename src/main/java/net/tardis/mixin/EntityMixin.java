package net.tardis.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.entity.Entity;
import net.tardis.mod.helper.WorldHelper;

@Mixin(Entity.class)
public class EntityMixin {
	/** Mixin into base Entity class to stop entities being removed if they fall below the world
	 * Logic in our helper method ensures this only occurs for item entities, non hostiles and players*/
	@Redirect(method = "outOfWorld()V", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;remove()V"))
    public void handleOutOfWorld(Entity entity) {
        WorldHelper.handleEntityOutOfWorld((Entity)(Object)this);
    }
}
