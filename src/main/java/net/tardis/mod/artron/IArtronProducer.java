package net.tardis.mod.artron;

public interface IArtronProducer {

    /**
     * returns amount to take
     * @param amt
     * @return
     */
    float takeArtron(float amt);
}
