package net.tardis.mod.contexts.gui;

import net.minecraft.client.Minecraft;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.guis.monitors.IMonitorGui;
import net.tardis.mod.misc.GuiContext;

public class MonitorContext extends GuiContext{

	int guiID;
	
	public MonitorContext(int guiID) {
		this.guiID = guiID;
	}
	
	public IMonitorGui getMonitor() {
		ClientHelper.openGUI(guiID, null);
		return (IMonitorGui)Minecraft.getInstance().currentScreen;
	}
}
