package net.tardis.mod.tags;

import net.minecraft.entity.EntityType;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.helper.Helper;

public class TardisEntityTypeTags {
   
   public static final ITag<EntityType<?>> IGNORED_ALARM_ENTITIES = makeEntity(Helper.createRL("ignored_alarm_entities"));
   public static final ITag<EntityType<?>> NOT_TAKEN_ON_LAND = makeEntity(Helper.createRL("not_taken_on_land"));

   public static ITag.INamedTag<EntityType<?>> makeEntity(ResourceLocation resourceLocation) {
        return EntityTypeTags.createOptional(resourceLocation);
   }

}
