package net.tardis.mod.recipe;

import java.util.Collection;

import com.google.gson.JsonObject;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.IngredientCodec;
import net.tardis.mod.tileentities.inventory.PanelInventoryWrapper;
/** Allows an item to be "attuned" via the Tardis Engine.
	 * <br> The input item can receive NBT tags with the Tardis' ID and Name information, or turned into a different item altogether*/
public class AttunableRecipe implements IRecipe<PanelInventoryWrapper>{
	/** Dummy recipe to to be used once a recipe has completed. It's initialised to prevent a hard crash from a Null Pointer Exception*/
	public static final AttunableRecipe EMPTY_RECIPE = new AttunableRecipe(false, 1, Ingredient.fromStacks(ItemStack.EMPTY), new RecipeResult(Items.AIR)).setRegistryId(new ResourceLocation(Tardis.MODID, "empty_attunable_recipe"));
	private ResourceLocation id;
	private int ticksToAttunement;
	private Ingredient ingredients;
	private RecipeResult result;
	private boolean addNBTTag = false;
	
	private static final Codec<AttunableRecipe> CODEC = RecordCodecBuilder.create(instance -> 
    instance.group(
    		Codec.BOOL.fieldOf("add_attunement_tags").forGetter(AttunableRecipe::shouldAddNBTTags), 
    		Codec.INT.fieldOf("attunement_ticks").forGetter(AttunableRecipe::getAttunementTicks),
    		IngredientCodec.INGREDIENT_CODEC.fieldOf("input").forGetter(AttunableRecipe::getInputIngredient),
    		RecipeResult.CODEC.fieldOf("result").forGetter(AttunableRecipe::getOutputResult)
    		).apply(instance, AttunableRecipe::new)
    );
	
	public static Collection<AttunableRecipe> getAllRecipes(World world){
        return world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.ATTUNEABLE_RECIPE_TYPE);
    }

	/**
	 * Allows an item to be "attuned" via the Tardis Engine.
	 * <br> The input item can receive NBT tags with the Tardis' ID and Name information, or turned into a different item altogether
	 * @param addNBTTag - If we should add Tardis World Key and Tardis Name NBT Tags to the result via {@linkplain AttunableItem#completeAttunement}
	 * @param ticksToAttunement - How many ticks should pass before the input item is turned into the result item
	 * @param singleInput - the input Ingredient. MUST BE A SINGLE ENTRY because the Attunement Container only has ONE slot for Attunement
	 * @param singleOutput - the output Item. Must be a single entry
	 */
	public AttunableRecipe(boolean addNBTTag, int ticksToAttunement, Ingredient singleInput, RecipeResult singleOutput) {
		this.ticksToAttunement = ticksToAttunement;
		this.ingredients = singleInput;
		this.result = singleOutput;
		this.addNBTTag = addNBTTag;
	}
	
	/**
	 * Convenience Constructor for recipes which are turning input item into a totally different output item
	 * <br> The addNBTTag is set to false here
	 * @param ticksToAttunement
	 * @param singleInput
	 * @param singleOutput
	 */
	public AttunableRecipe(int ticksToAttunement, Ingredient singleInput, RecipeResult singleOutput) {
	    this(false, ticksToAttunement, singleInput, singleOutput);
	}
    
	/**
	 * Get the number of ticks before the input item is converted to the result item
	 * @return
	 */
	public int getAttunementTicks() {
		return this.ticksToAttunement;
	}
	
	/** Gets the raw input Ingredient object
	 * <br> For a NonNullList of Ingredients for JEI/Display purposes, use {@link #getIngredients()}*/
	public Ingredient getInputIngredient() {
		return this.ingredients;
	}

	/** Gets the raw output result object
	 * <br> For the actual resulting ItemStack, use {@link #getRecipeOutput()}*/
	public RecipeResult getOutputResult() {
		return this.result;
	}

	/** Define if the result item should receive NBT tags that contain a Tardis' UUID World Key and user friendly name
	 * <p> Set this to false in the json file for all items that implement {@linkplain IAttunable} and have a capability
	 * <br> E.g. Stattenheim Remote and SonicItem
	 * <p> Set this to true in the json file if the item does not need a capability or any form of special data handling. 
	 * <br> E.g. Turning Diamonds to become "Imbued Diamonds" which have no need for storing Tardis data on it*/
	public boolean shouldAddNBTTags() {
		return this.addNBTTag;
	}
	
	public Codec<AttunableRecipe> getCodec(){
		return CODEC;
	}
	
	@Override
	public NonNullList<Ingredient> getIngredients() {
    	NonNullList<Ingredient> nonnulllist = NonNullList.create();
    	nonnulllist.add(this.ingredients);
		return nonnulllist;
	}
	
	@Override
	public boolean canFit(int width, int height) {
		return true;
	}

	@Override
	public ItemStack getRecipeOutput() {
		ItemStack resultStack = new ItemStack(this.result.getOutput());
		if (ItemStack.areItemsEqual(resultStack, this.ingredients.getMatchingStacks()[0])) {
			resultStack = this.ingredients.getMatchingStacks()[0].copy();
		}
		return resultStack;
	}

	@Override
	public ResourceLocation getId() {
		return this.id;
	}
	
	public AttunableRecipe setRegistryId(ResourceLocation id) {
        this.id = id;
        return this;
    }

	@Override
	public IRecipeSerializer<?> getSerializer() {
		return TardisRecipeSerialisers.ATTUNEABLE_SERIALISER.get();
	}

	@Override
	public IRecipeType<?> getType() {
		return TardisRecipeSerialisers.ATTUNEABLE_RECIPE_TYPE;
	}

	@Override
	public boolean matches(PanelInventoryWrapper inv, World worldIn) {
		if (inv.getPanelDirection() == Direction.EAST) { //If this is the refueler/attunement side
			ItemStack stackInSlot = inv.getStackInSlot(4);
			if (this.ingredients.test(stackInSlot)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	@Override
	public ItemStack getCraftingResult(PanelInventoryWrapper inv) {
		return ItemStack.EMPTY;
	}

	/** Wrapper class to allow for a "result" field where an inner json "item" field can be added to*/
    public static class RecipeResult{
        
        public static final Codec<RecipeResult> CODEC = RecordCodecBuilder.create(instance -> 
            instance.group(
                Registry.ITEM.fieldOf("item").forGetter(RecipeResult::getOutput)
            ).apply(instance, RecipeResult::new)
        );
        
        private Item output;
        
        public RecipeResult(Item output) {
            this.output = output;
        }
        
        public Item getOutput() {
            return this.output;
        }
        
        public void setOutput(Item input) {
            this.output = input;
        }
        
    }
	
	public static class AttunableRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> 
    implements IRecipeSerializer<AttunableRecipe>{

        @Override
        public AttunableRecipe read(ResourceLocation recipeId, JsonObject json) {
        	AttunableRecipe recipe = CODEC.parse(JsonOps.INSTANCE, json).resultOrPartial(Tardis.LOGGER::error).get();
            recipe.setRegistryId(recipeId); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public AttunableRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
        	AttunableRecipe recipe = CODEC.parse(NBTDynamicOps.INSTANCE, buffer.readCompoundTag()).resultOrPartial(Tardis.LOGGER::error).get();
            recipe.setRegistryId(recipeId); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public void write(PacketBuffer buffer, AttunableRecipe recipe) {
            buffer.writeCompoundTag((CompoundNBT) CODEC.encodeStart(NBTDynamicOps.INSTANCE, recipe).resultOrPartial(Tardis.LOGGER::error).orElse(new CompoundNBT()));
        }
    }

}
