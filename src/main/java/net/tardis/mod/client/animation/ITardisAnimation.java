package net.tardis.mod.client.animation;

import net.minecraft.nbt.INBT;
import net.minecraftforge.common.util.INBTSerializable;
/** Wrapper for IExteriorAnimation as a temp measure to solve a generics issue*/
public interface ITardisAnimation<T extends INBT> extends INBTSerializable<T>{

}
