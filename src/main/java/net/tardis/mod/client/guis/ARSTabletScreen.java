package net.tardis.mod.client.guis;

import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.ars.ARSPieceCategory;
import net.tardis.mod.client.guis.ars.ARSCategoryButton;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.sounds.TSounds;

public class ARSTabletScreen extends Screen {

    private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars_tablet.png");
    private List<ARSPieceCategory> categories = Lists.newArrayList();
    protected ARSPieceCategory openCategory = null;
    private static final int MAX_PER_PAGE = 6;

    private int index = 0;

    public ARSTabletScreen() {
        super(new StringTextComponent(""));
    }

    public ARSTabletScreen(GuiContext cont) {
        this();
    }

    @Override
    protected void init() {
        super.init();
        this.buttons.clear();
        this.addButtons(width / 2 - 100, height / 2 - 20);
        this.addDefaultButtons(width / 2 - 100, this.calculateMaxPageIndex());

    }

    public void addButtons(int x, int y) {
    	Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.SCREEN_BEEP_SINGLE.get(), 1.0F));
        this.categories.clear();
        //Build trees
        for(ResourceLocation loc : ARSPiece.getRegistry().keySet()){
            String[] path = loc.getPath().split("/");

            //Build categories out of all but the last
            ARSPieceCategory last = null;
            for(int i = 0; i < path.length - 1; ++i){
                String name = path[i];
                if(last == null)
                    last = this.getOrCreateCategories(name, loc.getNamespace());
                else last = last.add(name, loc.getNamespace());
            }
            //Add actual piece
            if(last != null)
                last.addPieceToList(ARSPiece.getRegistry().get(loc));

        }

        //Actually add category buttons
        int textSpacing = 0;
        int startIndex = index * MAX_PER_PAGE;
        int lastIndex = startIndex + MAX_PER_PAGE;
        lastIndex = lastIndex >= categories.size() ? categories.size() : lastIndex;
        
        if (startIndex < categories.size()) {
        	List<ARSPieceCategory> categoryPage = categories.subList(startIndex, lastIndex);
            
            for(ARSPieceCategory cat : categoryPage) {
                this.addButton(new ARSCategoryButton(x, y + (textSpacing * (this.font.FONT_HEIGHT + 2)), cat, this));
                ++textSpacing;
            }
        }
        else index = 0; //If the index exceeds the total size, reset the page index to 0
        

    }

    public ARSPieceCategory getOrCreateCategories(String name, String modid){
        for(ARSPieceCategory cat : categories){
            if(cat.categoryMatches(name, modid))
                return cat;
        }
        ARSPieceCategory cat = new ARSPieceCategory(name, modid);
        this.categories.add(cat);
        return cat;
    }

    public void modIndex(int mod, int size) {
        if (this.index + mod > size)
            this.index = 0;
        else if (this.index + mod < 0)
            this.index = size;
        else this.index += mod;

        this.init();
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {

        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        int width = 256, height = 173;
        this.blit(matrixStack, this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);

        super.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, new TranslationTextComponent(TardisConstants.Strings.GUI + "ars_tablet.gui_title").getString(), this.width / 2, this.height / 2 - (height / 2 - 30), 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, new TranslationTextComponent(TardisConstants.Strings.GUI + "ars_tablet.gui_info").getString(), this.width / 2, this.height / 2 - (height / 2 - 50), 0xFFFFFF);
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    public void openCategory(ARSPieceCategory cate){
        this.openCategory = cate;
        Minecraft.getInstance().displayGuiScreen(new ARSTabletCategoryScreen(this.openCategory));
    }

    protected void addDefaultButtons(int x, int maxIndex) {
        //Add next, prev, etc
    	this.addButton(new TextButton(x, height / 2 + 45, TardisConstants.Translations.GUI_NEXT.getString(), but -> modIndex(1, maxIndex)));
        this.addButton(new TextButton(x, height / 2 + 55, TardisConstants.Translations.GUI_PREV.getString(), but -> modIndex(-1, maxIndex)));
    }
    
    private int calculateMaxPageIndex() {
        int maxPageIndex = 0;
        
        if (!categories.isEmpty()) {
        	if (categories.size() % MAX_PER_PAGE == 0) {
            	maxPageIndex = this.categories.size() / MAX_PER_PAGE - 1;
            }
        	else {
        	    maxPageIndex = categories.size() / MAX_PER_PAGE;
        	}
        }
        
        return maxPageIndex;
    } 

}
