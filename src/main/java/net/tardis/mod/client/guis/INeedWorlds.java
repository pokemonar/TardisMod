package net.tardis.mod.client.guis;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.RegistryKey;
import net.minecraft.world.World;
/** Interface to allow a client side {@linkplain Screen} to receive World Keys
 * <br> Used for VM Teleport Screen*/
public interface INeedWorlds {

	abstract void setWorldsFromServer(List<RegistryKey<World>> worlds);
}
