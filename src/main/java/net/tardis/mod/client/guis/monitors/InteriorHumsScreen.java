package net.tardis.mod.client.guis.monitors;

import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeHumMessage;
import net.tardis.mod.registries.InteriorHumRegistry;
import net.tardis.mod.sounds.InteriorHum;

public class InteriorHumsScreen extends MonitorScreen {

    public InteriorHumsScreen(IMonitorGui screen, String menu) {
        super(screen, menu);
    }

    @Override
    protected void init() {
        super.init();

        int index = 0;
        for (InteriorHum hum : InteriorHumRegistry.HUM_REGISTRY.get().getValues()) {

            this.addButton(new TextButton(this.parent.getMinX(), this.parent.getMinY() - this.minecraft.fontRenderer.FONT_HEIGHT * index + 2, "> " + hum.getTranslatedName().getString(),
                    button -> Network.sendToServer(new ChangeHumMessage(hum, true))));
            ++index;
        }
    }
}
