package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.entity.Entity;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class AbstractConsoleEntityModel<T extends ConsoleTile> extends EntityModel<Entity>{

    public abstract void render(T tileEntity, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay, float red, float green, float blue, float alpha);

    @Override
    public void setRotationAngles(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
            float netHeadYaw, float headPitch) {
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
            float red, float green, float blue, float alpha) {
    }
}
