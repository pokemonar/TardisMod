package net.tardis.mod.client.models;

import java.util.function.Supplier;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.model.Model;
import net.tardis.mod.helper.TRenderHelper;

/**
 * Custom ModelRenderer to make individual cubes glow, instead of the whole bone that contains alot of cubes
 * <br> Useful for things like police box windows or lamps
 * @author Spectre0987
 *
 */
public class OldLightModelRenderer extends ModelRenderer{

	Supplier<Float> light;
	Supplier<Float> alpha;
	
	public OldLightModelRenderer(Model model, Supplier<Float> light) {
		super(model);
		this.light = light;
	}
	
	public OldLightModelRenderer(Model model, Supplier<Float> light, Supplier<Float> alpha) {
		this(model, light);
		this.alpha = alpha;
	}
	
	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn) {
		this.customRender(() -> super.render(matrixStackIn, bufferIn, packedOverlayIn, packedOverlayIn), matrixStackIn);
	}

	public void customRender(Runnable run, MatrixStack matrixStack) {
		matrixStack.push();
		float coord = 240.0F * light.get();
		TRenderHelper.setLightmapTextureCoords(coord, coord);
		
		run.run();
		
		TRenderHelper.restoreLightMap();
		matrixStack.pop();
	}

	

}
