package net.tardis.mod.sonic.interactions;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.constants.TardisConstants.Gui;
import net.tardis.mod.contexts.gui.GuiContextBlock;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.tileentities.machines.TransductionBarrierTile;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static net.minecraft.block.Blocks.REDSTONE_LAMP;
import static net.minecraft.block.DoorBlock.OPEN;
import static net.minecraft.block.RedstoneLampBlock.LIT;

/**
 * Created by Swirtzly
 * on 16/03/2020 @ 21:39
 */
public class SonicBlockInteraction extends AbstractSonicMode {

	private final Method dispenseMethod = ObfuscationReflectionHelper.findMethod(DispenserBlock.class, "func_176439_d", ServerWorld.class, BlockPos.class);


    @Override
    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
        World world = player.world;
        Block block = blockState.getBlock();

        //isHandActive checks if player hand is active (active hand refers to when the player has let go of right click)
        //This makes it so that i.e. player lets go of right click, onStoppedUsing will be called, so this method will be called, but ONLY if they let go of right click, instead of on right click
        if (!world.isRemote() && player.isHandActive()) {
            //It doesn't work on wood!
            if (isWood(blockState)) {
                PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.wood_fail"), true);
                return false;
            }

            if (block instanceof DispenserBlock) {
                try {
                    if (handleDischarge(player, sonic, 5)) {
                        dispenseMethod.invoke(world, pos);
                        return true;
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            //Explodes TNT
            if (block instanceof TNTBlock && TConfig.SERVER.detonateTnt.get()) {
                //TODO Cost 20
                if (handleDischarge(player, sonic, 20)) {
                    PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.detonate_tnt"), true);

                    block.catchFire(blockState, world, pos, player.getHorizontalFacing(), player);
                    world.removeBlock(pos, false);

                    /*
                    player.world.removeBlock(pos, true);
                    TNTEntity tnt = new TNTEntity(world, (double) ((float) pos.getX() + 0.5F), (double) pos.getY(), (double) ((float) pos.getZ() + 0.5F), player);
                    world.addEntity(tnt);
                    world.playSound(null, tnt.getPosX(), tnt.getPosY(), tnt.getPosZ(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
                     */
                    return true;
                }
            }

            //Toggle Redstone lamp
            if (block instanceof RedstoneLampBlock && TConfig.SERVER.redstoneLamps.get()) {
                if (block == REDSTONE_LAMP) {
                    if (handleDischarge(player, sonic, 5)) {
                        //TODO Cost 10
                        world.setBlockState(pos, blockState.with(LIT, !blockState.get(LIT)), 2); //Fixes neighbouring lamps from being turned off
                        return true;
                    }
                }
            }

            //Forcibly open a door
            if (block instanceof DoorBlock && TConfig.SERVER.openDoors.get()) {
                //TODO Cost 5
                if (handleDischarge(player, sonic, 5)) {
                    world.setBlockState(pos, blockState.with(OPEN, !blockState.get(OPEN)), 10);
                    world.playEvent(player, blockState.get(DoorBlock.OPEN) ? blockState.getMaterial() == Material.IRON ? 1005 : 1006 : blockState.getMaterial() == Material.IRON ? 1011 : 1012, pos, 0);
                    return true;
                }
            }

            //Force open a trap door
            if (block instanceof TrapDoorBlock && TConfig.SERVER.openTrapDoors.get()) {
                //TODO Cost 5
                if (handleDischarge(player, sonic, 5)) {
                    world.setBlockState(pos, blockState.with(OPEN, !blockState.get(OPEN)));
                    return true;
                }
            } else {
                PlayerHelper.sendMessageToPlayer(player, TardisConstants.Translations.INVALID_SONIC_RESULT, true);
            }
        }
        if (world.isRemote) { //Handle this in the transduction barrier
            if (world.getTileEntity(pos) instanceof TransductionBarrierTile) {
                if (handleDischarge(player, sonic, 0)) {
                    ClientHelper.openGUI(Gui.TRANSDUCTION_BARRIER_EDIT, new GuiContextBlock(world, pos));
                    return true;
                }
            } else {
                PlayerHelper.sendMessageToPlayer(player, TardisConstants.Translations.INVALID_SONIC_RESULT, true);
            }
        }

        return false;
    }

    @Override
    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
        return false;
    }

    @Override
    public ArrayList<TranslationTextComponent> getAdditionalInfo() {
        ArrayList<TranslationTextComponent> list = new ArrayList<>();
        list.add(new TranslationTextComponent("sonic.modes.info.interactable_blocks"));
        for (Block interactableBlock : getInteractableBlocks()) {
            list.add((TranslationTextComponent) new TranslationTextComponent("- ").appendSibling(new TranslationTextComponent(interactableBlock.getTranslationKey()).mergeStyle(TextFormatting.GRAY)));
        }
        return list;
    }

    public ArrayList<Block> getInteractableBlocks() { //TODO OPTIMISE, CACHE THIS!! May have to move to server starting event, although that isn't on the client...
        ArrayList<Block> list = new ArrayList<Block>();
        for (Block block : ForgeRegistries.BLOCKS) {
            if (block instanceof TrapDoorBlock || block instanceof DoorBlock || block instanceof RedstoneLampBlock || block instanceof TNTBlock || block == TBlocks.transduction_barrier.get()) {
                if (!isWood(block.getDefaultState())) {
                    list.add(block);
                }
            }
        }
        return list;
    }

    @Override
    public boolean hasAdditionalInfo() {
        return true;
    }

    @Override
    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {

    }

    public boolean isWood(BlockState blockState) {
        return blockState.getMaterial() == Material.WOOD || blockState.getMaterial() == Material.BAMBOO || blockState.getMaterial() == Material.BAMBOO_SAPLING;
    }

    @Override
    public void processSpecialBlocks(PlayerInteractEvent.RightClickBlock event) {
        World world = event.getWorld();
        BlockState blockState = world.getBlockState(event.getPos());
        if (blockState.getBlock() instanceof DispenserBlock) {
            event.setCanceled(true);
        }
        processBlock(event.getPlayer(), blockState, event.getItemStack(), event.getPos());

    }

}
