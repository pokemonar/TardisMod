package net.tardis.mod.misc.quantiscope;

import net.tardis.mod.containers.QuantiscopeContainer;
import net.tardis.mod.containers.QuantiscopeSonicContainer;
import net.tardis.mod.tileentities.machines.QuantiscopeTile;

public class SonicSlotMapper extends QuantiscopeSlotMapper {

    @Override
    public void addSlots(QuantiscopeContainer container, QuantiscopeTile tile) {
        container.addSlot(new QuantiscopeSonicContainer.SonicSlot(tile, 0, 42, 47));
    }

}
