package net.tardis.mod.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.RegistryKey;
import net.minecraft.world.World;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.tileentities.ConsoleTile;

public class ArtronTrail implements INBTSerializable<CompoundNBT>, IEncodable {

    private double artronStrength = 0;
    private RegistryKey<World> console;
    private String tardisName;

    public double getArtronStrength(){
        return this.artronStrength;
    }

    @Override
    public CompoundNBT serializeNBT() {
        return null;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {

    }

    @Override
    public void encode(PacketBuffer buf) {

    }

    @Override
    public void decode(PacketBuffer buf) {

    }

    public static ArtronTrail createTrail(ConsoleTile tile){
        if(tile.getWorld() != null){
            
        }
        return null;
    }
}
