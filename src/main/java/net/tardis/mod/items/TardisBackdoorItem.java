package net.tardis.mod.items;

import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.DoorBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.state.properties.DoubleBlockHalf;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.entity.TardisBackdoorEntity;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.misc.ConsoleBoundWithTooltipItem;
import net.tardis.mod.properties.Prop;

public class TardisBackdoorItem extends ConsoleBoundWithTooltipItem {
    
    private static final IFormattableTextComponent DESCRIPTION = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.backdoor.description"));
    
    public TardisBackdoorItem(){
        super(Prop.Items.ONE.get());
        this.setHasDescriptionTooltips(true);
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        if (!context.getWorld().isRemote()) {
            BlockState doorState = context.getWorld().getBlockState(context.getPos());
            if(context.getWorld().getBlockState(context.getPos()).getBlock().isIn(BlockTags.DOORS)){
                if(doorState.hasProperty(DoorBlock.HALF)){
                    if (doorState.get(DoorBlock.HALF) == DoubleBlockHalf.UPPER) {
                        TardisBackdoorEntity backdoorEnt = new TardisBackdoorEntity(context.getWorld());
                        if (this.getTardis(context.getItem()) != null) {
                            RegistryKey<World> intDimKey = WorldHelper.getWorldKeyFromRL(this.getTardis(context.getItem()));
                            backdoorEnt.setInteriorWorldKey(intDimKey);
                            backdoorEnt.setPosition(context.getPos().getX() + 0.5, context.getPos().getY() - 1, context.getPos().getZ() + 0.5);
                            context.getWorld().addEntity(backdoorEnt);
                            context.getPlayer().world.playSound(null, context.getPlayer().getPosition(), SoundEvents.BLOCK_PORTAL_TRIGGER, SoundCategory.BLOCKS, 1F, 1F);
                            if (!context.getPlayer().abilities.isCreativeMode)
                                context.getItem().shrink(1);
                        }
                    }                     
                }
                return ActionResultType.SUCCESS;
            }
        }
        return ActionResultType.PASS;
    }

    @Override
    public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
            ITooltipFlag flagIn) {
        super.createDescriptionTooltips(stack, worldIn, tooltip, flagIn);
        tooltip.add(DESCRIPTION);
    }
}
