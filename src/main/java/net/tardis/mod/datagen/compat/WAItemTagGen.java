package net.tardis.mod.datagen.compat;

import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraft.item.Item;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.KeyItem;

import static net.tardis.mod.tags.TardisItemTags.makeItem;

/* Created by Craig on 16/02/2021 */
public class WAItemTagGen extends ItemTagsProvider {

    //Safe to have here as it isn't used by TM code anywhere
    public static final ITag.INamedTag<Item> ANGEL_THEFT = makeItem(new ResourceLocation("weeping_angels", "angel_theft"));


    public WAItemTagGen(DataGenerator dataGenerator, BlockTagsProvider blockTagProvider , ExistingFileHelper existingFileHelper) {
        super(dataGenerator, blockTagProvider, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        for (Item item : ForgeRegistries.ITEMS.getValues()) {
            if(item instanceof KeyItem){
                add(ANGEL_THEFT, item);
            }
        }
    }

    public void add(ITag.INamedTag< Item > branch, Item block) {
        this.getOrCreateBuilder(branch).add(block);
    }
}
