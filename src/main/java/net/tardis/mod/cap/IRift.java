package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.artron.IArtronProducer;

public interface IRift extends IArtronProducer, INBTSerializable<CompoundNBT> {

    void addEnergy(float energy);

    float getRiftEnergy();

    boolean isRift();

    void setRift(boolean rift);

    void tick();
    /** 1.17: IStorage interface removed with no replacement. Move these methods to the provider that implements {@linkplain ICapabilitySerializable}*/
    @Deprecated
    public static class Storage implements IStorage<IRift> {

        @Override
        public INBT writeNBT(Capability<IRift> capability, IRift instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<IRift> capability, IRift instance, Direction side, INBT nbt) {
            instance.deserializeNBT((CompoundNBT) nbt);
        }

    }

    public static class Provider implements ICapabilitySerializable<CompoundNBT> {

        final IRift rift;
        final LazyOptional<IRift> provide;

        public Provider(IRift rift) {
            this.rift = rift;
            this.provide = LazyOptional.of(() -> this.rift);
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == Capabilities.RIFT ? provide.cast() : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return rift.serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            rift.deserializeNBT(nbt);
        }

    }

}
