package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.missions.MiniMission;

import javax.annotation.Nullable;
import java.util.List;

public interface IMissionCap extends INBTSerializable<CompoundNBT> {

    @Nullable
    MiniMission getMissionForPos(BlockPos pos);

    void addMission(MiniMission mission);

    List<MiniMission> getAllMissions();

    void tick(World world);
    
    /** 1.17: IStorage interface removed with no replacement. Move these methods to the provider that implements {@linkplain ICapabilitySerializable}*/
    @Deprecated
    public static class Storage implements Capability.IStorage<IMissionCap> {

        @Override
        public INBT writeNBT(Capability<IMissionCap> capability, IMissionCap instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<IMissionCap> capability, IMissionCap instance, Direction side, INBT nbt) {
            instance.deserializeNBT((CompoundNBT) nbt);
        }

    }

    public static class Provider implements ICapabilitySerializable<CompoundNBT> {

        private final LazyOptional<IMissionCap> holder;
        private IMissionCap cap;

        public Provider(IMissionCap cap) {
            this.cap = cap;
            this.holder = LazyOptional.of(() -> cap);
        }

        @Override
        public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
            return cap == Capabilities.MISSION ? holder.cast() : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return cap.serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            cap.deserializeNBT(nbt);
        }

    }
}
