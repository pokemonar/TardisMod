package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.controls.AbstractControl;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateControlMessage;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.tileentities.ConsoleTile;

public class ControlEntity extends Entity{

	private static final DataParameter<String> CONTROL = EntityDataManager.createKey(ControlEntity.class, DataSerializers.STRING);
	private AbstractControl controlObject;
	private ConsoleTile console;
	private boolean shouldTick = false;
	private boolean firstTick = true;
	
	private int timeTillUpdate = 30;
	
	public ControlEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		
	}

	public ControlEntity(World world) {
		this(TEntities.CONTROL.get(), world);
	}

	/*
	 * Must be done before spawning, unless you're an idiot
	 */
	public void setControl(AbstractControl control) {
		this.controlObject = control;
		if(!world.isRemote && control != null) {
			this.dataManager.set(CONTROL, control.getEntry().getRegistryName().toString());
			this.updateControlValues();
		}
	}
	
	public AbstractControl getControl() {
		if(this.controlObject == null) {
			ControlEntry entry = ControlRegistry.getControl(new ResourceLocation(this.getDataManager().get(CONTROL)));
			if(entry != null)
				return (this.controlObject = entry.spawn(this.getConsole(), this));
		}
		return this.controlObject;
	}
	
	public void updateControlValues() {
		this.recalculateSize();
		this.shouldTick = this.getControl() instanceof ITickable;
	}
	
	@Override
	protected void registerData() {
		this.getDataManager().register(CONTROL, this.controlObject != null ? controlObject.getEntry().getRegistryName().toString() : "");
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(CONTROL, compound.getString("control"));
		this.getControl().deserializeNBT(compound.getCompound("control_data"));
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putString("control", this.getControl().getEntry().getRegistryName().toString());
		compound.put("control_data", this.controlObject.serializeNBT());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		
		if(!this.world.isRemote && this.getControl() == null)
			remove();
		
		if(this.shouldTick)
			((ITickable) this.getControl()).tick(this.getConsole());
		
		if(this.ticksExisted % 20 == 0) {
			if(!world.isRemote)
				this.getDataManager().set(CONTROL, this.getControl().getEntry().getRegistryName().toString());
			this.updateControlValues();
		}
		
		if(this.getControl() != null && this.getControl().getAnimationTicks() > 0)
			this.getControl().setAnimationTicks(this.getControl().getAnimationTicks() - 1);
		
		if(this.ticksExisted == 5) {
			if(!world.isRemote)
				Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
		}
		
		if(timeTillUpdate > 0) {
			--timeTillUpdate;
			if(this.timeTillUpdate == 0 && !world.isRemote)
				Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
		}
		
		if(!world.isRemote && this.ticksExisted > 20 && this.getControl() != null && this.getControl().isDirty()) {
			Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
			this.getControl().clean();
		}
		
		if(this.firstTick) {
			this.timeTillUpdate = 20;
			this.firstTick = false;
		}
	}

	@Override
	public EntitySize getSize(Pose poseIn) {
		
		if(this.getConsole() != null && !this.getConsole().isRemoved() && this.controlObject != null) {
			if(this.getConsole().getControlOverrides().containsKey(this.controlObject.getClass())) {
				return this.getConsole().getControlOverrides().get(this.controlObject.getClass()).getSize();
			}
		}
		
		return this.getControl() == null ? super.getSize(poseIn) : this.getControl().getSize();
	}
    
	/**
	 * This is the correct method needed to sync between server and client and prevents our control methods being fired twice
	 * <br> It's used by the PlayerController and ServerPlayNetHandler (Server connection)
	 */
	@Override
	public ActionResultType applyPlayerInteraction(PlayerEntity player, Vector3d vec, Hand hand) {
		if(!this.isControlVaild())
			return ActionResultType.FAIL;
		
		//Only preocess once
		if(hand != Hand.MAIN_HAND) {
			return ActionResultType.PASS;
		}
		

		//Flight Events
		FlightEvent current = this.getConsole().getFlightEvent();
		if (current != null) {
			if(current.onControlHit(this.getConsole(), getControl())) {
				this.setAnimationTicks(this.getControl().getMaxAnimationTicks());
				this.world.playSound(null, this.getPosition(), this.getControl().getSuccessSound(getConsole()), SoundCategory.PLAYERS, 1F, 1F);
				return ActionResultType.PASS;
			}
		}
		
		boolean worked = this.getControl().onRightClicked(this.getConsole(), player);
		world.playSound(null, getPosition(), worked ? this.getControl().getSuccessSound(this.getConsole()) : this.getControl().getFailSound(this.getConsole()), SoundCategory.BLOCKS, 1, 1);
		if(!world.isRemote)
			Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
		this.getConsole().setPilot(player);
		return ActionResultType.SUCCESS;
	}

	@Override
	public boolean hitByEntity(Entity entityIn) {
		if(!this.isControlVaild())
			return false;

		//Flight Events
		FlightEvent current = this.getConsole().getFlightEvent();
		if (current != null) {
			if(current.onControlHit(this.getConsole(), getControl())) {
				this.setAnimationTicks(this.getControl().getMaxAnimationTicks());
				this.world.playSound(null, this.getPosition(), this.getControl().getSuccessSound(getConsole()), SoundCategory.PLAYERS, 1F, 1F);
				return true;
			}
		}
		
		if(entityIn instanceof PlayerEntity) {
			boolean worked = this.getControl().onHit(this.getConsole(), (PlayerEntity)entityIn);
			if(!world.isRemote) {
				world.playSound(null, getPosition(), worked ? this.getControl().getSuccessSound(this.getConsole()) : this.getControl().getFailSound(getConsole()), SoundCategory.BLOCKS, 1, 1);
				Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
			}
			this.getConsole().setPilot((PlayerEntity)entityIn);
		}
		return super.hitByEntity(entityIn);
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public boolean canBeAttackedWithItem() {
		return true;
	}

	@Override
	public ITextComponent getDisplayName() {
		return controlObject == null ? new StringTextComponent("None") : this.getControl().getDisplayName();
	}
	
	private ConsoleTile getConsole() {
		return this.console;
	}
	
	public void setConsole(ConsoleTile console) {
		this.console = console;
		if(this.getControl() != null)
			this.getControl().setConsole(console, this);
	}
	
	public void setAnimationTicks(int ticks) {
		this.getControl().setAnimationTicks(ticks);
		if (!world.isRemote)
			Network.sendPacketToAll(new UpdateControlMessage(this.getEntityId(), this.getControl().serializeNBT(), this.getControl().getAnimationTicks()));
	}
	
	@Override
	public void onAddedToWorld() {
		super.onAddedToWorld();
		timeTillUpdate = 30;
	}
	
	public boolean isControlVaild(){
		if(this.getControl() == null)
			return false;
		return this.getControl().getConsole() != null && this.getControl().getConsole().hasWorld();
	}
	

	@Override
	public boolean canRenderOnFire() {
		return false;
	}
}
