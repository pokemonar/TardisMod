package net.tardis.mod.entity.humanoid;

import javax.annotation.Nullable;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.OpenDoorGoal;
import net.minecraft.entity.ai.goal.RandomWalkingGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants.Gui;
import net.tardis.mod.contexts.gui.EntityContext;
import net.tardis.mod.missions.misc.Dialog;

public abstract class AbstractHumanoidEntity extends CreatureEntity{
	
	public AbstractHumanoidEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}

	@Override
	public boolean preventDespawn() {
		return true;
	}

    @Override
	public ActionResultType getEntityInteractionResult(PlayerEntity player, Hand hand) {
		if(world.isRemote && this.getCurrentDialog(player) != null) {
			ClientHelper.openGUI(Gui.DIALOG, new EntityContext(this));
			return ActionResultType.SUCCESS;
		}
		else {
			return ActionResultType.FAIL;
		}
	}
    
    public static AttributeModifierMap.MutableAttribute createAttributes() {
    	return CreatureEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 20D)
    			.createMutableAttribute(Attributes.ARMOR_TOUGHNESS, 10D);
    }

    /** Gets the current dialog and options for this player. <br> Must be called on the logical Client-Side*/
	@Nullable
	public abstract Dialog getCurrentDialog(PlayerEntity player);
	public abstract ResourceLocation getSkin();
	
	@Override
	public double getYOffset() {
		return -0.35D;
	}
	
	public void startUseAnimation() {
		this.swingArm(Hand.MAIN_HAND);
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		//Overrides
		this.goalSelector.addGoal(2, new LookAtGoal(this, PlayerEntity.class, 2.0F));
		
		
		//Normal
		this.goalSelector.addGoal(3, new SwimGoal(this));
		this.goalSelector.addGoal(4, new RandomWalkingGoal(this, 0.3));
		this.goalSelector.addGoal(5, new OpenDoorGoal(this, true));
		
	}
	
}
