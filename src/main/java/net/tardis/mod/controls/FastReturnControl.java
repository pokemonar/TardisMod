package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class FastReturnControl extends BaseControl{

	public FastReturnControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.099999994F, 0.099999994F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.2F, 0.325F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		return EntitySize.flexible(0.125F, 0.125F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote() && console.getLandTime() <= 0) {
			SpaceTimeCoord coord = console.getReturnLocation();
			RegistryKey<World> worldKey = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, coord.getDimRL());
			console.setDestination(worldKey, coord.getPos());
			console.setExteriorFacingDirection(coord.getFacing());
			this.startAnimation();
		}
		return true;
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(9 / 16.0, 7 / 16.0, 10.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.26433123161703187, 0.45625, -0.8173110443482853);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(-0.3252235365186568, 0.15, -1.05);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(0.002, 0.438, 0.375);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.206, 0.531, -0.617);
		
		if (this.getConsole() instanceof XionConsoleTile)	
			return new Vector3d(-0.28484833473880096, 0.71875, -0.5101038312508607);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(-0.7006116964503074, 0.46875, -0.6819940772792219);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.8303029743618082, 0.46875, -0.15396611984219444);
		
		return new Vector3d(16.0 / 16.0, 7.0 / 16.0, 2.5 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE.get();
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}
}
