package net.tardis.mod.traits;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class ClaustrophobicTrait extends TardisTrait{

	public static final TranslationTextComponent CLOSED_IN = new TranslationTextComponent(createTranslationKey(false, Helper.createRL("claustrophobic")));

	public ClaustrophobicTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {

		if(tile.getWorld().getGameTime() % 200 == 0){
			tile.getOrFindExteriorTile().ifPresent(ext -> {
				final int totalBlocks = 16 * 16 * 16;
				int airCount = 0;
				for(int x = -8; x < 8; ++x){
					for(int z = -8; z < 8; ++z){
						for(int y = 0; y < 16; ++y){
							if(ext.getWorld().isAirBlock(ext.getPos().add(x, y, z))){
								++airCount;
							}
						}
					}
				}
				final float airPercent = airCount / (float)totalBlocks;
				if(airPercent < 0.5){
					this.decreaseMood(tile);
				}
			});
		}
		
	}

	public void decreaseMood(ConsoleTile tile){
		tile.getEmotionHandler().addMood(-1 + (this.getModifier() * 9));
		this.warnPlayerLooped(tile, CLOSED_IN, 400);
	}

}
