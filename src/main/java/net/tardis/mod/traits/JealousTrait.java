package net.tardis.mod.traits;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

import java.util.UUID;

public class JealousTrait extends TardisTrait{

    public static final TranslationTextComponent LEFT_OUT = new TranslationTextComponent(createTranslationKey(false, Helper.createRL("jealous")));
    public static final TranslationTextComponent SPENDING_TIME = new TranslationTextComponent(createTranslationKey(true, Helper.createRL("enjoying_time")));

    public JealousTrait(TardisTraitType type) {
        super(type);
    }

    @Override
    public void tick(ConsoleTile tile) {
        if(!tile.getWorld().isRemote()) {
            if(tile.getWorld().getGameTime() % 200 == 0) {
                for (UUID uuid : tile.getEmotionHandler().getLoyaltyTrackingCrew()) {
                    ServerPlayerEntity player = tile.getWorld().getServer().getPlayerList().getPlayerByUUID(uuid);
                    if (player != null) {
                        //If one of the crew is flying another Tardis, lower mood and loyalty
                        if (TardisHelper.isInATardis(player) && !player.getServerWorld().getDimensionKey().equals(tile.getWorld().getDimensionKey())) {
                            TardisHelper.getConsoleInWorld(player.getServerWorld()).ifPresent(otherTardis -> {
                                if (otherTardis.isInFlight() && tile.getEmotionHandler().getMood() > EnumHappyState.SAD.getTreshold()) {
                                    double mod = this.getModifier();
                                    tile.getEmotionHandler().addMood(-(1 * (int)Math.round(4 * mod)));
                                    tile.getEmotionHandler().addLoyalty(uuid, -(1 + (int)Math.round(4 * mod)));
                                    this.warnPlayer(tile, LEFT_OUT);
                                }
                            });
                        }
                        //If the pilot of this Tardis is one of the loyalty tracked crew, increase mood
                        if(tile.isInFlight() && tile.getPilot() == player && tile.getEmotionHandler().getMood() < EnumHappyState.HAPPY.getTreshold()) {
                            tile.getEmotionHandler().addMood(10);
                            this.warnPlayer(tile, SPENDING_TIME);
                        }
                    }
                }
            }
        }
    }
}
