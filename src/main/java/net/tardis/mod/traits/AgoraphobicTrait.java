package net.tardis.mod.traits;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class AgoraphobicTrait extends TardisTrait{

    public static final TranslationTextComponent TOO_OPEN = new TranslationTextComponent(createTranslationKey(false, Helper.createRL("agoraphobic")));

    public AgoraphobicTrait(TardisTraitType type) {
        super(type);
        
    }

    @Override
    public void tick(ConsoleTile tile) {
        if(tile.getWorld().getGameTime() % 200 == 0) {
        	tile.getOrFindExteriorTile().ifPresent(ext -> {
        		//Trait that dislikes the outdoors or open spaces.
        		//If the top of the exterior block can see the sky, lose mood
				if(ext.getWorld().canBlockSeeSky(ext.getPos()) && tile.getEmotionHandler().getMood() > EnumHappyState.DISCONTENT.getTreshold()) {
					tile.getEmotionHandler().addMood(-1 + (this.getModifier() * 9));
					this.warnPlayerLooped(tile, TOO_OPEN, 400);
				}
			});
        }
    }

}
