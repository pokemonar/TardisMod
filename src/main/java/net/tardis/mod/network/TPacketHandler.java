package net.tardis.mod.network;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.registries.TardisStatistics;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.world.dimensions.TDimensions;
/** Util class for handling server side packets*/
public class TPacketHandler {
    
	public static void handleVortexMTeleport(ServerPlayerEntity sender, BlockPos dest, boolean teleportPrecise) {
		handleVortexMTeleport(sender, dest, sender.getEntityWorld().getDimensionKey(), false, teleportPrecise);
	}
	
    public static void handleVortexMTeleport(ServerPlayerEntity sender, BlockPos dest, RegistryKey<World> destWorld, boolean isInterdimensional, boolean teleportPrecise) {
        ServerWorld world = sender.getServer().getWorld(destWorld);
    	if (!LandingSystem.isPosBelowOrAboveWorld(world, dest.getY())
                 && LandingSystem.isBlockWithinWorldBorder(world, dest.getX(), dest.getY(), dest.getZ()) 
                 && WorldHelper.canVMTravelToDimension(world)) {
             double diff = Math.sqrt(sender.getPosition().distanceSq(dest)); //Calculates difference between dest and current pos
             float fuelDischarge = (float) (TConfig.SERVER.vmBaseFuelUsage.get() + (TConfig.SERVER.vmFuelUsageMultiplier.get() * diff)); //Scales discharge amount
             //This checks for which itemstack we want to open the container for
             //If we are holding a VM, use it as the itemstack. Otherwise, get the stack closest to slot 0
             ItemStack vm = new ItemStack(TItems.VORTEX_MANIP.get());
             ItemStack stack = PlayerHelper.getHeldOrNearestStack(sender, vm);
             
             stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> {
	            	 float fuelUsage = fuelDischarge;
	            	 boolean shouldAddPenalty = isInterdimensional && destWorld != sender.world.getDimensionKey();
	            	 double extraDistance = shouldAddPenalty ? diff * 2 : 0;
            	     if (shouldAddPenalty) {
            	    	 fuelUsage *= 1.5F;
	                 }
            	     int numSeconds = TConfig.SERVER.vmFuelUsageTime.get();
            	     double yLevel = 500 + (0.5 * diff) + extraDistance - 10; //Minus 10 because we start to teleport the player at y level 10 in the vortex dimension to prevent void damage
            	     double freeFallVelocityIdealPerTick = -((Math.floor(0.98D) - 1) * 3.92D);
            	     double freeFallVelocityIdealSec = freeFallVelocityIdealPerTick * 20D;
            	     float timeToDrain = (float)(((float)(yLevel/freeFallVelocityIdealSec)) / (float)numSeconds);
            	     float trueFuelUsage = fuelUsage * timeToDrain; //Calculate an estimate of total fuel usage over the length of free fall in the vortex dimension. Can be subject to Artron battery discharge multiplier, so the actual fuel use is usually lower than estimate
                     if (cap.getTotalCurrentCharge() >= trueFuelUsage) { //Don't let them teleport if they don't have enough fuel to start the journey
                         cap.setDischargeAmount(fuelUsage);
                         BlockPos destination = teleportPrecise ? dest : LandingSystem.getTopBlock(sender.world, dest); //Ensures the vm will tp you to a safe spot
                         SpaceTimeCoord coord = new SpaceTimeCoord(destWorld, destination);
                         sender.getCapability(Capabilities.PLAYER_DATA).ifPresent(playerCap -> {
                             playerCap.setDestination(coord);
                             playerCap.calcDisplacement(sender.getPosition(), destination);
                         });
                         sender.getServerWorld().playSound(null, sender.getPosition(), TSounds.VM_TELEPORT.get(), SoundCategory.BLOCKS, 0.25F, 1F); //Play sound at start position
                         cap.setVmUsed(true); //Tell server that we used the VM to teleport into Vortex Dim
                         Helper.addTardisStatistic(sender, TardisStatistics.VORTEX_TRAVEL_COUNT);
                         sender.getServer().enqueue(new TickDelayedTask(1, () -> {
                        	 WorldHelper.teleportEntities(sender, sender.world.getServer().getWorld(TDimensions.VORTEX_DIM), new BlockPos(0, yLevel, 0), 0, 90); 
                         }));
                     }
                     else {
                         sender.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.fuel_empty", trueFuelUsage)), false);
                     }
                 }); 
             }
         else {
             sender.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.invalidPos")), false);
         }
    }

}
