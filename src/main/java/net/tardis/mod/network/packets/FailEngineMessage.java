package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class FailEngineMessage {

	int slot = 0;
	Direction dir;
	
	public FailEngineMessage(Direction dir, int slot) {
		this.slot = slot;
		this.dir = dir;
	}
	
	public static void encode(FailEngineMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.dir.getIndex());
		buf.writeInt(mes.slot);
	}
	
	public static FailEngineMessage decode(PacketBuffer buf) {
		return new FailEngineMessage(Direction.byIndex(buf.readInt()), buf.readInt());
	}
	
	public static void handle(FailEngineMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			ServerPlayerEntity player = cont.get().getSender();
			player.getServerWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
				PanelInventory inv = data.getEngineInventoryForSide(mes.dir);
				InventoryHelper.spawnItemStack(player.getServerWorld(), player.getPosX(), player.getPosY(), player.getPosZ(), inv.getStackInSlot(mes.slot));
				inv.setStackInSlot(mes.slot, ItemStack.EMPTY);
				player.attackEntityFrom(TDamageSources.CIRCUITS, 1);
			});
		});
		cont.get().setPacketHandled(true);
	}
}
