package net.tardis.mod.network.packets.data.Requestors;

import java.util.Optional;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.tileentity.TileEntity;
/** Wrapper for a tile specific update packets*/
public abstract class Requestor<T extends TileEntity> {

    private Class<T> validClass;
    private int id;

    public Requestor(Class<T> validClass){
        this.validClass = validClass;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public abstract void act(T tile, ServerPlayerEntity sender);

    public void tryToActOnTile(TileEntity tile, ServerPlayerEntity sender){
        isValid(tile).ifPresent(te ->{
                this.act(te, sender);
        });
    }

    public Optional<T> isValid(TileEntity te){
//        System.out.println("Checking " + te + " valid");
        if(this.validClass.isInstance(te))
            return Optional.of(cast(te));
        return Optional.empty();
    }

    public T cast(TileEntity te){
        return this.validClass.cast(te);
    }

}
