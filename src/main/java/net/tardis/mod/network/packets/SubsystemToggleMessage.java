package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;

public class SubsystemToggleMessage {

	private ResourceLocation key;
	private boolean isSubsystem;
	boolean activated;
	
	public SubsystemToggleMessage(ResourceLocation key, boolean subsystem, boolean activated) {
		this.key = key;
		this.isSubsystem = subsystem;
		this.activated = activated;
	}
	
	public static void encode(SubsystemToggleMessage mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.key);
		buf.writeBoolean(mes.isSubsystem);
		buf.writeBoolean(mes.activated);
	}
	
	public static SubsystemToggleMessage decode(PacketBuffer buf){
		return new SubsystemToggleMessage(buf.readResourceLocation(), buf.readBoolean(), buf.readBoolean());
	}
	
	public static void handle(SubsystemToggleMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(context.get().getSender().getServerWorld()).ifPresent(tile -> {
				if(mes.isSubsystem){
					tile.getSubsystem(mes.key).ifPresent(sys -> {
						sys.setActivated(mes.activated);
					});
				}
				else{
					tile.getUpgrade(mes.key).ifPresent(upgrade -> {
						upgrade.setActivated(mes.activated);
						upgrade.update();
					});
				}
			});
		});
		context.get().setPacketHandled(true);
	}
}
